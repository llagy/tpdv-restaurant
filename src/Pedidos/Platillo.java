package Pedidos;

import Inventario.almacen.Ingrediente;
import java.util.ArrayList;

// <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
// #[regen=yes,id=DCE.94837845-6464-6539-7E72-2169EF2994DB]
// </editor-fold> 
public class Platillo {

    //TODO agregar tipo de platillo : int con setter y getter
    //TODO agregar clase sugerenciaDelChef (menú del día) como subclase de Platillo
    private ArrayList<Ingrediente> ingredientesDelPlatillo;
    //TODO observacoines únicamente para para el chef como : termino medio
    //String observaciones;

    private double precio;
    private double iva;
    private String nombrePlatillo;
    private String categoria;
    int cantidad;

    public Platillo() {
        cantidad = 1;
    }

    public String getNombrePlatillo() {
        return nombrePlatillo;
    }

    public void setNombrePlatillo(String val) {
        this.nombrePlatillo = val;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double val) {
        this.precio = val;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public ArrayList<Ingrediente> getIngredientesDelPlatillo() {
        return ingredientesDelPlatillo;
    }

    public void setIngredientesDelPlatillo(ArrayList<Ingrediente> val) {
        this.ingredientesDelPlatillo = val;
    }

//    public String getObservaciones() {
//        return observaciones;
//    }
//
//    public void setObservaciones(String observaciones) {
//        this.observaciones = observaciones;
//    }
    public double getIva() {
        return iva;
    }

    public void setIva(double iva) {
        this.iva = iva;
    }

    public void imprimirEnPantalla() {
        System.out.println(nombrePlatillo + " - " + precio + " - " + iva);
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public void agregarMás(int numero) {
        cantidad += numero;
    }
}
