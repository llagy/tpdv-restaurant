package Pedidos;

import Administracion.Mesero;
import ingresosyegresos.Descuento;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Vector;

/**
 * Clase que modela una orden tomada para el usuario y una comanda para chef
 */
public class Comanda {

    private double precio;

    private int numeroDePedido;

    private Date fechaDePedido;

    private boolean cerrada;

    private Mesero quienTomoLaOrden;

    private ArrayList<Platillo> platillos;
    //TODO tal vez se encesite ponerle el id de comanda para buscarlo después

    //TODO ligar observaciones al menú, de pperefencia del cajero
    /**
     * Este campo es para cuando se deja sin cobrar por alguna razón
     */
    String Ovservaciones;

    /**
     * <p style="margin-top: 0">
     * Este puede ser un descuento adicional para clientes frecuentes
     * </p>
     */
    private ArrayList<Descuento> descuentoAdicional;

    public Comanda() {
        platillos = new ArrayList<Platillo>();
        Ovservaciones = "Ninguna";
    }

    public double obtenerSubtotal() {
        return 0.0;
    }

    public String obtenListaDePlatillos() {
        return null;
    }

    public boolean getCerrada() {
        return cerrada;
    }

    public void setCerrada(boolean val) {
        this.cerrada = val;
    }

    public ArrayList<Descuento> getDescuentoAdicional() {
        return descuentoAdicional;
    }

    public void setDescuentoAdicional(ArrayList<Descuento> val) {
        this.descuentoAdicional = val;
    }

    public Date getFechaDePedido() {
        return fechaDePedido;
    }

    public void setFechaDePedido(Date val) {
        this.fechaDePedido = val;
    }

    public int getNumeroDePedido() {
        return numeroDePedido;
    }

    public void setNumeroDePedido(int val) {
        this.numeroDePedido = val;
    }

    public ArrayList<Platillo> getPlatillos() {
        return platillos;
    }

    public Vector<Platillo> getPlatillosVec() {
        Vector<Platillo> vectplatillos = new Vector<Platillo>();
        for (Platillo platilloFeliz : platillos) {
            vectplatillos.add(platilloFeliz);
        }
        return vectplatillos;
    }

    public void setPlatillos(ArrayList<Platillo> val) {
        this.platillos = val;
    }

    public Mesero getQuienTomoLaOrden() {
        return quienTomoLaOrden;
    }

    public void setQuienTomoLaOrden(Mesero val) {
        this.quienTomoLaOrden = val;
    }

    public double getPrecio() {
        //calcular precio
        precio = 0;
        for (Platillo next : platillos) {
            int precioplatillo = 0;
            precioplatillo += next.getPrecio();
            precioplatillo += next.getIva();
            precioplatillo *= next.cantidad;
            precio += precioplatillo;
        }
        return precio;
    }

    public void setPrecio(double val) {
        this.precio = val;
    }

    public String getOvservaciones() {
        return Ovservaciones;
    }

    public void setOvservaciones(String Ovservaciones) {
        this.Ovservaciones = Ovservaciones;
    }

    /**
     * Método que permite agregar un nuevo platillo a la comanda
     *
     * @param platilloAAgregar El platillo a agregar
     */
    public void addPlatillo(Platillo platilloAAgregar) {
        platillos.add(platilloAAgregar);

    }

    public void mostrarPlatillosEnPantalla() {
        for (Platillo platillo : platillos) {
            platillo.imprimirEnPantalla();
        }
    }
}
