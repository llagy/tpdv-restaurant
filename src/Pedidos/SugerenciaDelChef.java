/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pedidos;

import java.util.Vector;

/**
 * Provee la capacidad de agregar varios platillos en descuento
 *
 * @author Administrador
 */
public class SugerenciaDelChef extends Platillo {

    Vector<Platillo> platillos = new Vector<Platillo>();

    public SugerenciaDelChef(double precio) {
        setNombrePlatillo("Sugerencia del chef");
        setPrecio(precio);
    }

    public Vector<Platillo> getPlatillos() {
        return platillos;
    }

    public void setPlatillos(Vector<Platillo> platillos) {
        this.platillos = platillos;
    }

    public synchronized boolean add(Platillo e) {
        return platillos.add(e);
    }

    public boolean remove(Object o) {
        return platillos.remove(o);
    }

}
