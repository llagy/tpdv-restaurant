package Pedidos;

import java.util.ArrayList;

// <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
// #[regen=yes,id=DCE.28A237B8-BBA9-A99B-A40D-A3AE53BDFF58]
// </editor-fold> 
public class Mesa {

    int numeroDeMesa;
    int estadoMesa;
    private boolean estaOcupada;
    private boolean estaEnLimpieza;
    private ArrayList<Comanda> pedidosDeLaMEsa;

    public Mesa() {
    }

    public boolean getEstaOcupada() {
        return estaOcupada;
    }

    public void setEstaOcupada(boolean val) {
        this.estaOcupada = val;
    }

    public ArrayList<Comanda> getPedidosDeLaMEsa() {
        return pedidosDeLaMEsa;
    }

    public void setPedidosDeLaMEsa(ArrayList<Comanda> val) {
        this.pedidosDeLaMEsa = val;
    }

    public boolean isEstaEnLimpieza() {
        return estaEnLimpieza;
    }

    public void setEstaEnLimpieza(boolean estaEnLimpieza) {
        this.estaEnLimpieza = estaEnLimpieza;
    }

    public int getNumeroDeMesa() {
        return numeroDeMesa;
    }

    public void setNumeroDeMesa(int numeroDeMesa) {
        this.numeroDeMesa = numeroDeMesa;
    }

    public int getEstadoMesa() {
        return estadoMesa;
    }

    public void setEstadoMesa(int estadoMesa) {
        this.estadoMesa = estadoMesa;
        //TODO que en base al estado se ponga ocupada o no
    }
}
