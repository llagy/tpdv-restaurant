package Administracion;

import Pedidos.Platillo;
import SistemaDeKioscos.ServicioDeAccesoABaseDeDatos;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.JOptionPane;

// <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
// #[regen=yes,id=DCE.ECA74E9E-14C5-F3E6-676C-79451FC8F025]
// </editor-fold> 
/**
 * El administrador tiene el poder de modificar precios o modificar el
 * inventario puede también cobrar ingresar facturas revisar proveedores
 */
public class Administrador {

    private ArrayList<Mesero> meserosEmpleados;

    private ArrayList<Platillo> listaDePlatillos;

    ServicioDeAccesoABaseDeDatos baseDeDatos;

    public Administrador() {
        baseDeDatos = new ServicioDeAccesoABaseDeDatos();
        listaDePlatillos = new ArrayList<Platillo>();
    }

    public ArrayList<Platillo> getListaDePlatillos() {
        return listaDePlatillos;
    }

    public void setListaDePlatillos(ArrayList<Platillo> val) {
        this.listaDePlatillos = val;
    }

    public ArrayList<Mesero> getMeserosEmpleados() {
        return meserosEmpleados;
    }

    public void setMeserosEmpleados(ArrayList<Mesero> val) {
        this.meserosEmpleados = val;
    }

    public void agregarMesero() {
        String usuario = JOptionPane.showInputDialog("Escriba el Nombre de Usuario"); //TODO tal vez sea mejor un cuadro de diálogo
        String contraseña = JOptionPane.showInputDialog("Escriba la contraseña de su Usuario");
        baseDeDatos.agregarNuevoUsuario(usuario, contraseña);
        //TODO verificar pedir datos personales
        String nombre = JOptionPane.showInputDialog("Escriba el Nombre de Mesero");
        String direccion = JOptionPane.showInputDialog("Escriba la dirección del Mesero");
        String telefono = JOptionPane.showInputDialog("Escriba el teléfono del Mesero");
        String correo = JOptionPane.showInputDialog("Escriba el e-mail");
        baseDeDatos.agregarDatosDeUsuario(usuario, nombre, direccion, telefono, correo);
    }

    public void quitarMesero() {
        String usuario = JOptionPane.showInputDialog("Escriba el nombre de usuario a eliminar");
        baseDeDatos.eliminarUsuario(null);
    }

    public void cambiarMeseroDeComanda() {
        //TODO cambiar quien atiende la mesa
    }

    public void traspasarComandaAOtraMesa() {
        //TODO para cuando los clientes se cambian demesa
    }

    public void AgregarNuevoPlatillo(Platillo platillo) {
        listaDePlatillos.add(platillo);
    }

    public void realizarCorteDeCaja() {
        //TODO corte de caja aquí
        //TODO avisar de faltantes en $$
    }

    public void consultarCortesDeCAjaPasados() {

    }

    public ServicioDeAccesoABaseDeDatos getBaseDeDatos() {
        return baseDeDatos;
    }

    public void setBaseDeDatos(ServicioDeAccesoABaseDeDatos baseDeDatos) {
        this.baseDeDatos = baseDeDatos;
    }

    public void modificarNickMesero() {
        String usuario = JOptionPane.showInputDialog("Escriba el Nombre de Usuario");
        String nuevoUsuario = JOptionPane.showInputDialog("Escriba el Nuevo Nombre de Usuario");
        baseDeDatos.cambiarNombreDeUsuario(usuario, nuevoUsuario);
    }

    public void modificarPasswordMesero() {
        String usuario = JOptionPane.showInputDialog("Escriba el Nombre de Usuario");
        String nuevopass = JOptionPane.showInputDialog("Escriba el Nuevo Password de Usuario");
        baseDeDatos.cambiarPasswordDeUsuario(usuario, nuevopass);
    }

    public void quitarPlatillo() {
        String nombreDePlatllo = JOptionPane.showInputDialog("Escriba el Nombre de Platillo");
        baseDeDatos.quitarPlatillo(nombreDePlatllo);
        listaDePlatillos.remove(buscarPlatillo(nombreDePlatllo));
    }

    private Platillo buscarPlatillo(String nombreDePlatllo) {
        for (Platillo next : listaDePlatillos) {
            if (next.getNombrePlatillo() == nombreDePlatllo) {
                return next;
            }
        }
        return null;
    }

    public void modificarPasswordAdmin() {
        modificarPasswordMesero();//TODO agregar una tabla de categorías para controlar niveles de acceso
    }

    public void agregarAdministrador(String usuario, String contras) {
        usuario = JOptionPane.showInputDialog("Escriba el Nombre de Usuario"); //TODO tal vez sea mejor un cuadro de diálogo
        contras = JOptionPane.showInputDialog("Escriba la contraseña de su Usuario");
        baseDeDatos.agregarNuevoUsuario(usuario, contras);
        //TODO verificar pedir datos personales
        String nombre = JOptionPane.showInputDialog("Escriba el Nombre de Mesero");
        String direccion = JOptionPane.showInputDialog("Escriba la dirección del Mesero");
        String telefono = JOptionPane.showInputDialog("Escriba el teléfono del Mesero");
        String correo = JOptionPane.showInputDialog("Escriba el e-mail");
        baseDeDatos.agregarDatosDeUsuario(usuario, nombre, direccion, telefono, correo);
        //agregar privilegios de administrador
        baseDeDatos.agregarPrivilegiosDeAdministrador(usuario);
    }

}
