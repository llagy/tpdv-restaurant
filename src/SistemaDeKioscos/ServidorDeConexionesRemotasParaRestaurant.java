/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SistemaDeKioscos;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Administrador
 */
public class ServidorDeConexionesRemotasParaRestaurant {

    //Tributo a Mao ;) aún faltan dos dígitos porque el 2401 es para descubrimiento de servicios tcp/ip

    ServerSocket socketDeServidor;
    Socket conexionDeCliente;

    public ServidorDeConexionesRemotasParaRestaurant() {
        
        try {
            socketDeServidor = new ServerSocket(30051, 10);//
            while (true) {
                esperarConexiones();
                obtenerCanalesDeComunicacion();
                interactuar();
            }
        } catch (IOException ex) {
            Logger.getLogger(ServidorDeConexionesRemotasParaRestaurant.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void esperarConexiones() {
        try {
            System.out.println("Escuchando a posibles clientes...");
            conexionDeCliente = socketDeServidor.accept();
            String nombreMesero = conexionDeCliente.getInetAddress().getHostName();
            ServicioDeNotificacion.alertaPersonalizada("Mesero comunicandose", nombreMesero + " se comunicará con la BD por favor no apague el equipo");
            System.out.println("Conexión exitosa de " + nombreMesero);
        } catch (IOException ex) {
            Logger.getLogger(ServidorDeConexionesRemotasParaRestaurant.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    BufferedReader entrada;
    PrintWriter salida;
    private void obtenerCanalesDeComunicacion() {
        try {
            //TODO definir que tipo de datos enviarán los meseros
            //pra mandar y recibir puro texto
             salida = new PrintWriter(conexionDeCliente.getOutputStream());
             entrada = new BufferedReader(new InputStreamReader(conexionDeCliente.getInputStream()));
            System.out.println("Listo para recibir mensajes");
            
        } catch (IOException ex) {
            Logger.getLogger(ServidorDeConexionesRemotasParaRestaurant.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void interactuar() {
        salida.write("Hola mesero");
            salida.flush();
     try {
        String mensajeMesero="";
//        ServicioDeAccesoABaseDeDatos baseDeDatos = new ServicioDeAccesoABaseDeDatos();
//        baseDeDatos.conectarABaseDeDatos();
        do {            
           
                mensajeMesero = entrada.readLine();
            System.out.println(mensajeMesero);
        } while (!mensajeMesero.equals("POST"));
        //TODO definir protocolo de comunicación para
        //TODO recibir mesa ocupada
        //TODO recibir mensaje de mesa para limpiar
        //TODO recibir mensaje de mesa libre
        //TODO recibir mensaje de imprimir orden en cocina
        //TODO recibir mensaje de prepararse para cobrar cuenta?
//        baseDeDatos.desconectarDeBaseDeDatos();
        } catch (IOException ex) {
                Logger.getLogger(ServidorDeConexionesRemotasParaRestaurant.class.getName()).log(Level.SEVERE, null, ex);
            }
    }
    
    public static void main(String[] args) {
        ServidorDeConexionesRemotasParaRestaurant sdcrpr = new ServidorDeConexionesRemotasParaRestaurant();
        
    }
}
