/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package SistemaDeKioscos;

import GUI.ventanaNotificaciones;

/**
 *
 * @author Administrador
 */
public class ServicioDeNotificacion {
    //TODO Mandar una notificación diferente segun el tipo
    /* Tipos de mensajes de notificación sugeridos
     * al dueño cada que se registre un pago.
     * al chef cada que los ingredientes estén por acabarse
     * al chef cada que los ingredientes se hayan acabado
     * al chef cada que las bebidas estén por acabarse
     * cada que se mande una comanda a la cocina
     * cada que el sistema saque a alguien por inactividad
     * Mensaje de Bienvenida
     */
    
    /**
     * método que muestra una ventana de bienvenida
     */
    public static void bienvenida(){
        new ventanaNotificaciones().setVisible(true);
    }
    
    public static void ingredientesAgotados(String ingrediente) {
        new ventanaNotificaciones("Ingredientes Agotados","Lo sentimos pero el ingrediente "+ingrediente+" está a punto de agotarse").setVisible(true);
    }
    
    /**
     * Método que muestra una alerta personalizada
     * @param titulo El tíulo de la ventana
     * @param mensaje El mensaje soporta HTML4
     */
    public static void alertaPersonalizada(String titulo,String mensaje) {
        //TODO tal vez se vería mejor con opacidad 50 setOpacity(0.5f);
        new ventanaNotificaciones(titulo,mensaje).setVisible(true);
    }
}
