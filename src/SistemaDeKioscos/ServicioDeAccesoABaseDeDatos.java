package SistemaDeKioscos;

import Administracion.Mesero;
import Inventario.almacen.Ingrediente;
import Pedidos.Comanda;
import Pedidos.Mesa;
import Pedidos.Platillo;
import com.mysql.jdbc.CommunicationsException;
import java.net.ConnectException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;

/**
 * Clase que provee de una manera más segura la conexión, envío de mensajes
 * predeterminados y la desconexión de una base de datos
 */
public class ServicioDeAccesoABaseDeDatos {

    public static String ruta;
    static Connection conexion;
    Statement consulta;
    private ResultSet resultados;

    /////////// valores fijos con nombre ////////////////////
    public static final int SINDATOS = -1;
    public static final int MESA_DESOCUPADA = 0;
    public final int MESA_OCUPADA = 1;
    /**
     * Indica que un usuario no tiene privilegios en el sistema
     */
    public final int SINPRIVILEGIOS = 2;
    /**
     * Indica que un usuario tiene privilegios de mesero en el sistema
     */
    public final int MESERO = 3;
    public final int ADMINISTRADOR = 4;

    public ServicioDeAccesoABaseDeDatos() {

        //ruta = "ciaovela.db";
        //ruta = "//localhost/bellissima"; //´para usuario any
        ruta = "//localhost/bellissima?user=ciao&password=bella";
    }

    public void actualizarPlatillo(String nombre, String precio, String iva, String categoria) {
        int id = obtenerIDDePlatillo(nombre);
        conectarABaseDeDatos();
        try {
            consulta = conexion.createStatement();
            consulta.executeUpdate("UPDATE platillo SET precio = '" + precio + "',iva='" + iva + "',categoria ='" + categoria + "' WHERE id = '" + id + "'");
        } catch (SQLException ex) {
            Logger.getLogger(ServicioDeAccesoABaseDeDatos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void agregarDatosDeUsuario(String usuario, String nombre, String direccion, String telefono, String email) {
        int id = obtenerIDDeUsuario(usuario);
        conectarABaseDeDatos();
        try {
            consulta = conexion.createStatement();
            consulta.executeUpdate("INSERT IGNORE INTO info_personal VALUES(NULL,'" + id + "','" + nombre + "','" + direccion + "','" + telefono + "','" + email + "')");
        } catch (SQLException ex) {
            Logger.getLogger(ServicioDeAccesoABaseDeDatos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Método que nos permite aregar un nuevo ingrediente a la base de datos
     *
     * @param nombre Nombre del ingrediente
     * @param caducidad Fecha de caducidad
     * @param veces Veces que se puede usar
     * @param descripcion Aquí podemos poner cosas como la presentación, la
     * marca, etc
     */
    public void agregarNuevoIngrediente(String nombre, String caducidad, int veces, String descripcion) {
        conectarABaseDeDatos();
        try {
            consulta = conexion.createStatement();
            consulta.executeUpdate("INSERT INTO ingrediente VALUES('', '" + nombre + "', '" + caducidad + "','" + veces + "', " + descripcion + ")");
        } catch (SQLException ex) {
            Logger.getLogger(ServicioDeAccesoABaseDeDatos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

        }
    }

    public void agregarNuevoPlatillo(String platillo, double precio, double iva, String catego) {
        conectarABaseDeDatos();
        try {
            consulta = conexion.createStatement();
            String strQuery = "INSERT INTO platillo VALUES(NULL, '" + platillo + "', '" + precio + "','" + iva + "', '" + catego + "')";
            System.out.println(strQuery);
            consulta.executeUpdate(strQuery);
        } catch (SQLException ex) {
            Logger.getLogger(ServicioDeAccesoABaseDeDatos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

        }
    }

    /**
     * Permite agregar un nuevo mesero a la BD
     *
     * @param usuario el nombre de usuario
     * @param contraseña la contraseña de acceso
     */
    public void agregarNuevoUsuario(String usuario, String contraseña) {
        conectarABaseDeDatos();
        //TODO cambiar el diseño de bd para que el id sea autoincremental
        try {
            consulta = conexion.createStatement();
            consulta.executeUpdate("INSERT INTO usuario VALUES(''," + usuario + ",  " + contraseña + ")");
        } catch (SQLException ex) {
            Logger.getLogger(ServicioDeAccesoABaseDeDatos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

        }
    }

    public void agregarPlatilloAComanda(Platillo p, Comanda orden, int idDeComanda) {
        //)
        int idplatillo = obtenerIDDePlatillo(p.getNombrePlatillo());
        //obtenerIDDeComanda();
        conectarABaseDeDatos();
        try {
            consulta = conexion.createStatement();
            String strQuery = "INSERT INTO comanda_has_platillo VALUES('" + idDeComanda + "','" + idplatillo + "','" + p.getCantidad() + "')";
            System.out.println(strQuery);
            consulta.executeUpdate(strQuery);
        } catch (SQLException ex) {
            Logger.getLogger(ServicioDeAccesoABaseDeDatos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void atenderMesa(Mesero mesero, int numeroDeMesa) {

        //////////// obtener id de usuario ////////////////////
        int idusuario = -1;
        conectarABaseDeDatos();
        try {
            // consulta = conexion.createStatement();
            resultados = consulta.executeQuery("SELECT id FROM usuario u WHERE nombre = '" + mesero.getNombre().trim() + "'");
            resultados.next();
            idusuario = resultados.getInt("id");
            System.out.println("Id de usuario:" + idusuario);
        } catch (SQLException ex) {
            Logger.getLogger(ServicioDeAccesoABaseDeDatos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

        }
        if (idusuario == -1) {
            System.err.println("problema con el id de usuario");
            return;
        }

        //////////// Apartar Mesa /////////////////////////
        conectarABaseDeDatos();
        try {
            System.out.println("Apartando la mesa " + numeroDeMesa + " ...");
            //consulta = conexion.createStatement();//TODO tal vez se necesite omitir esto
            consulta.executeUpdate("UPDATE mesa SET estado = '" + MESA_OCUPADA + "' WHERE nomesa ='" + numeroDeMesa + "'");
        } catch (SQLException ex) {
            Logger.getLogger(ServicioDeAccesoABaseDeDatos.class.getName()).log(Level.SEVERE, null, ex);

            //si hay excepción aquí es que no se modificó la base de datos
        } finally {

        }

        ////////// Asignar mesa a mesero //////////// 
        conectarABaseDeDatos();
        try {
            System.out.println("Reservando la mesa...");
            consulta = conexion.createStatement();
            consulta.executeUpdate("Insert IGNORE into usuario_has_mesa values('" + idusuario + "','" + numeroDeMesa + "')");
        } catch (SQLException ex) {
            Logger.getLogger(ServicioDeAccesoABaseDeDatos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

        }
    }

    private void atenderMesa(Mesero mesero, String nombreMesa) {
        //////////// obtener id de usuario ////////////////////
        int idusuario = -1;
        conectarABaseDeDatos();
        try {
            consulta = conexion.createStatement();
            System.out.println("Nombre de mesero:\"" + mesero.getNombre().trim() + "\"");
            String consultastr = "SELECT * FROM usuario WHERE nombre = '" + mesero.getNombre() + "'";
            System.out.println("*" + consultastr + "*");
            resultados = consulta.executeQuery(consultastr);
            ResultSetMetaData rsmd = resultados.getMetaData();
            int numberOfColumns = rsmd.getColumnCount();
            if (numberOfColumns < 1) {
                idusuario = resultados.getInt("id");
            } else {
                System.err.println("Sin resulados");
            }

        } catch (SQLException ex) {
            //no ha sido entregada a nadie
            Logger.getLogger(ServicioDeAccesoABaseDeDatos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

        }
        if (idusuario == -1) {
            System.err.println("problema con el id de usuario");
            return;
        }

        //////////// Apartar Mesa /////////////////////////
        conectarABaseDeDatos();
        try {
            consulta = conexion.createStatement();
            resultados = consulta.executeQuery("UPDATE mesa SET estado = '" + MESA_OCUPADA + "' WHERE nomesa ='" + nombreMesa + "'");
        } catch (SQLException ex) {
            Logger.getLogger(ServicioDeAccesoABaseDeDatos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

        }

        ////////// Asignar mesa a mesero //////////// 
        conectarABaseDeDatos();
        try {
            consulta = conexion.createStatement();
            consulta.executeUpdate("Insert into usuario_has_mesa values('" + idusuario + "','" + nombreMesa + "')");
        } catch (SQLException ex) {
            Logger.getLogger(ServicioDeAccesoABaseDeDatos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

        }
    }

    /**
     * Método que nos permite obtener todos los platillos con nombre similar al
     * que especifiquemos
     *
     * @param nombrePlatillo El nombre de platillo a buscar
     */
    public Platillo buscarPlatillo(String nombrePlatillo) {
        try {
            conectarABaseDeDatos();

            consulta = conexion.createStatement();
            resultados = consulta.executeQuery("SELECT * FROM platillo WHERE nombre like '%" + nombrePlatillo + "%'");
            resultados.last();//nos pasamos al último
            ResultSetMetaData rsmd = resultados.getMetaData();
            int numCols = rsmd.getColumnCount();
            System.out.println("Columnas del resultado" + numCols);

            int numFils = resultados.getRow();

            // int j = 0;
            resultados.beforeFirst();//mover antes de empezar
            //String nombrePlatillo = resultados.getString(2);
            //id nombre precio categoria
            resultados.next();
            nombrePlatillo = resultados.getString("nombre");
            int precio = resultados.getInt("precio");
            int iva = resultados.getInt("iva");
            String categoria = resultados.getString("categoria");
            Platillo p = new Platillo();
            p.setNombrePlatillo(nombrePlatillo);
            p.setPrecio(precio);
            p.setIva(iva);
            p.setCategoria(categoria);
            return p;
        } catch (SQLException ex) {
            Logger.getLogger(ServicioDeAccesoABaseDeDatos.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void cambiarNombreDePlatillo(String nombreAnterior, String nuevoNombre) {
        int id = obtenerIDDePlatillo(nombreAnterior);
        conectarABaseDeDatos();
        try {
            consulta = conexion.createStatement();
            consulta.executeUpdate("UPDATE platillo SET nombre = '" + nuevoNombre + "' WHERE id = '" + id + "'");
        } catch (SQLException ex) {
            Logger.getLogger(ServicioDeAccesoABaseDeDatos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Método que nos permite cambiar el nombre con que inicia sesión un usuario
     * del sistema
     *
     * @param usuario nombre anterior
     * @param nuevoUsuario el nombre que tendrá
     */
    public void cambiarNombreDeUsuario(String usuario, String nuevoUsuario) {
        conectarABaseDeDatos();
        try {
            consulta = conexion.createStatement();
            resultados = consulta.executeQuery("UPDATE usuario SET nombre = '" + nuevoUsuario + "' WHERE nombre = '" + usuario + "'");
        } catch (SQLException ex) {
            Logger.getLogger(ServicioDeAccesoABaseDeDatos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

        }
    }

    public void cambiarPasswordDeUsuario(String usuario, String nuevopass) {
        conectarABaseDeDatos();
        try {
            consulta = conexion.createStatement();
            resultados = consulta.executeQuery("UPDATE usuario SET password = '" + nuevopass + "' WHERE nombre = '" + usuario + "'");
        } catch (SQLException ex) {
            Logger.getLogger(ServicioDeAccesoABaseDeDatos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

        }
    }

    public void cerrarComanda(int id) {
        conectarABaseDeDatos();
        try {
            consulta = conexion.createStatement();
            consulta.executeUpdate("UPDATE comanda SET cerrada = '1' WHERE idComanda = '" + id + "'");
        } catch (SQLException ex) {
            Logger.getLogger(ServicioDeAccesoABaseDeDatos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

        }
    }

    public boolean comprobarSiAtiendeMesa(Mesero mesero, int nomesa) {
        conectarABaseDeDatos();
        try {
            consulta = conexion.createStatement();
            resultados = consulta.executeQuery("SELECT * FROM usuario_has_mesa WHERE usuario_id = '" + getIDMesero(mesero) + "' AND mesa_nomesa='" + nomesa + "'");
            if (resultados.isLast()) {//esta mesa está siendo atendida por nosotros si no por alguien más
                return false;
            }

        } catch (SQLException ex) {
            Logger.getLogger(ServicioDeAccesoABaseDeDatos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } finally {
            return true;
        }
    }

    /**
     * Método que permite comprobar si una mesa está disponible
     *
     * @param nombreMesa El nombre de mesa un # para mesa barra# para barra
     * @param m Εl mesero que atiende esta mesa
     * @return Regresa Regresa el estado o -1 si algo falla
     */
    public int compruebaEstadoMesa(int nombreMesa, Mesero m) {
        conectarABaseDeDatos();
        int estado = SINDATOS;
        try {
            //consulta = conexion.createStatement();
            String consultaS = "SELECT estado FROM mesa WHERE nomesa ='" + nombreMesa + "'";
            System.out.println(consultaS);
            resultados = consulta.executeQuery(consultaS);
            ResultSetMetaData rsmd = resultados.getMetaData();
            int numberOfColumns = rsmd.getColumnCount();
            System.out.println("Numero de columnas:" + numberOfColumns);
            System.out.println(rsmd.getCatalogName(1));
            System.out.println(rsmd.getColumnName(1));
            System.out.println(rsmd.getColumnClassName(1));
            System.out.println(rsmd.getColumnTypeName(1));
            resultados.next();
            System.out.println("fila actual" + resultados.getRow());
            System.out.println(resultados.getInt(1));
            estado = resultados.getInt(1);
            System.out.println("Estado actual de la mesa:" + estado);
        } catch (SQLException ex) {
            try {
                Logger.getLogger(ServicioDeAccesoABaseDeDatos.class.getName()).log(Level.SEVERE, null, ex);
                System.err.println("No se había reservado mesa");
                //no se puede consultar porque la tabla no existe
                consulta = conexion.createStatement();
                //UPDATE mesa SET estado = '1' WHERE nomesa ='$mesa'
                //SELECT id FROM usuario u WHERE nombre = '$nombremesero'
                System.out.println(m.getNombre() + " atendiendo");
                atenderMesa(m, nombreMesa);
                //Insert into usuario_has_mesa values('$idmesero','$mesa')
                // ResultSet rs = consulta.executeQuery("UPDATE mesa SET estado = '1' WHERE nomesa ='" + nombreMesa + "'");
                //rs = consulta.executeQuery("SELECT id FROM usuario u WHERE nombre = '" + m.getNombre() + "'");
                //int idmesero = rs.getInt("id");
                //rs = consulta.executeQuery("Insert into usuario_has_mesa values('" + idmesero + "','" + nombreMesa + "'");
            } catch (SQLException ex1) {
                Logger.getLogger(ServicioDeAccesoABaseDeDatos.class.getName()).log(Level.SEVERE, null, ex1);
            }
        } finally {

        }
        return estado;
    }

    /**
     * Este método nos permitirá conectarnos localmente a una base de datos
     */
    public void conectarABaseDeDatos() {
        try {
            System.out.println("conectando...");
            //Class.forName("org.sqlite.JDBC"); //conexión con BD sqlite
            Class.forName("com.mysql.jdbc.Driver");
            System.out.println("Clase hallada");
            //conexion = DriverManager.getConnection("jdbc:sqlite:" + ruta);
            conexion = DriverManager.getConnection("jdbc:mysql:" + ruta);
            System.out.println("conectado...");
        } catch (ClassNotFoundException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        } catch (SQLException ex) {
            System.err.println("No se pudo conectar a la base de datos");
            ServicioDeNotificacion.alertaPersonalizada("Error", "No se pudo conectar a la base de datos");
        }
    }

    public void desReservarMesa(Mesero mesero, int numeroDeMesa) {
        conectarABaseDeDatos();
        try {
            consulta = conexion.createStatement();
            resultados = consulta.executeQuery("UPDATE mesa SET estado = '0' WHERE nomesa ='" + numeroDeMesa + "'");//no atendida
            int idusuario = getIDMesero(mesero);
            resultados = consulta.executeQuery("Delete from usuario_has_mesa WHERE usuario_id='" + idusuario + "' AND mesa_nomesa='" + numeroDeMesa + "'");
        } catch (SQLException ex) {
            Logger.getLogger(ServicioDeAccesoABaseDeDatos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

        }
    }

    public void EliminarConexionABaseDeDatos() {
        if (resultados != null) {
            try {
                resultados.close();
            } catch (SQLException e) {
                System.err.println("Problemas al cerrar los sultados");
            }
        }
        if (consulta != null) {
            try {
                consulta.close();
            } catch (SQLException e) {
                System.err.println("Problemas al cerarr la consulta");
            }
        }
        if (conexion != null) {
            try {
                conexion.close();
            } catch (SQLException e) {
                System.err.println("Problemas al cerrar la base de datos");
            }
        }
    }

    public void eliminarIngrediente(String nombre) {
        conectarABaseDeDatos();
        try {
            consulta = conexion.createStatement();
            resultados = consulta.executeQuery("DELETE FROM ingrediente WHERE nombre ='" + nombre + "'");
        } catch (SQLException ex) {
            Logger.getLogger(ServicioDeAccesoABaseDeDatos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

        }
    }

    public void eliminarPlatillo(String platillo) {
        conectarABaseDeDatos();
        try {
            consulta = conexion.createStatement();
            resultados = consulta.executeQuery("DELETE FROM platillo WHERE nombre ='" + platillo + "'");
        } catch (SQLException ex) {
            Logger.getLogger(ServicioDeAccesoABaseDeDatos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

        }
    }

    /**
     * Método que borra un usuario de la BD ojo, este es peligroso, lo ideal es
     * desactivarlos
     *
     * @param usuario nickname
     */
    public void eliminarUsuario(String usuario) {
        conectarABaseDeDatos();
        try {
            consulta = conexion.createStatement();
            resultados = consulta.executeQuery("DELETE FROM usuario WHERE nombre ='" + usuario + "'");
        } catch (SQLException ex) {
            Logger.getLogger(ServicioDeAccesoABaseDeDatos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

        }
    }

    private int getIDMesero(Mesero mesero) {
        conectarABaseDeDatos();
        try {
            consulta = conexion.createStatement();
            Date hoy = new Date();
            if (mesero.getNombre() == null) {
                System.err.println("Mesero sin nombre");
                return -1;//solo en caso de que se esté corriendo sin correr todo el menú
            }
            String strQuery = "SELECT id FROM usuario u WHERE nombre = '" + mesero.getNombre() + "'";
            System.out.println(strQuery);
            resultados = consulta.executeQuery(strQuery);
            ResultSetMetaData rsmd = resultados.getMetaData();
            int numberOfColumns = rsmd.getColumnCount();
            if (resultados.wasNull()) {//if (numberOfColumns > 0) {
                System.err.println("Resultados nulos");
            } else {
                resultados.next();
                return resultados.getInt(1);//("id");
            }
        } catch (SQLException ex) {
            Logger.getLogger(ServicioDeAccesoABaseDeDatos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

        }
        return -1;
    }

    public void imprimirTicketConCuenta(String nameLocal, String expedition, String box, String ticket, String caissier, String dateTime, String items, String subTotal, String tax, String total) {
        Ticket miticket = new Ticket(nameLocal, expedition, box, ticket, caissier, dateTime, items, subTotal, tax, total);

    }

    /**
     * Realiza un logueo de cualquier usuario
     *
     * @param usuario nickname
     * @param pass password
     * @return is there's a match into DB
     */
    public boolean login(String usuario, String pass) {
        try {
            consulta = conexion.createStatement();
            resultados = consulta.executeQuery("Select * from usuario Where nombre='" + usuario.trim() + "' and password='" + pass.trim() + "'");
            return resultados.next();
        } catch (SQLException ex) {
            ServicioDeNotificacion.alertaPersonalizada("Problema con la base de datos", "No se puede conectar a la base de datos");
            Logger.getLogger(ServicioDeAccesoABaseDeDatos.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    /**
     * M[etodo que permite comprobar si un usuario logueado tiene alg[un
     * privilegio de acceso
     *
     * @param usuario El Nickname del usuario
     * @param pass El password del usuario
     * @return regresa si el usuaroi está logueado
     * @See #MESERO
     */
    public int loginUsuario(String usuario, String pass) {
        if (login(usuario, pass)) {
            try {
                consulta = conexion.createStatement();
                //TODO consultar usuario y privilegios
                resultados = consulta.executeQuery("SELECT nombreRol FROM roles INNER JOIN usuario ON (roles.usuario_id = usuario.id) WHERE nombre='" + usuario.trim() + "' and password='" + pass.trim() + "'");
                resultados.next();
                String privilegios = resultados.getString(1);
                System.out.println("Privilegos en BD:" + privilegios);
                if ("Administrador".equals(privilegios)) {
                    System.out.println("Admin");
                    return ADMINISTRADOR;
                } else {
                    System.out.println("Mesero");
                    return MESERO;
                }
            } catch (SQLException ex) {
                Logger.getLogger(ServicioDeAccesoABaseDeDatos.class.getName()).log(Level.SEVERE, null, ex);
            }

            //TODO preguntar en la base de datos los privilegios del usuario
            //si es usuario regresar 2
            //si es admin regresar 1
        }
        return SINPRIVILEGIOS;
    }

    public int[] nuevaComandaConPlatillos(Comanda orden, Mesero mesero, int numeroDeMesa, int recomendacionesDeChefPorApuntar) {
        conectarABaseDeDatos();
        int[] ids = new int[2];
        try {
            consulta = conexion.createStatement();
            Date hoy = new Date();
            String fechaDeHoy = new SimpleDateFormat("YYYY/MM/dd").format(hoy);
            int idmesero = getIDMesero(mesero);
            String strQuery = "INSERT IGNORE INTO comanda VALUES('','" + fechaDeHoy + "','0','" + orden.getPrecio() + "','" + orden.getOvservaciones() + "','" + idmesero + "','" + numeroDeMesa + "')";
            System.out.println(strQuery);
            consulta.executeUpdate(strQuery);// lo inserta solo la primera vez
            resultados = consulta.executeQuery("SELECT LAST_INSERT_ID()");// lo inserta solo la primera vez
            //resultados = consulta.executeQuery("SELECT id FROM comanda WHERE fecha='" + fechaDeHoy + "' and cerrada='0' and precio='" + orden.getPrecio() + "' and observaciones ='" + orden.getOvservaciones() + "' and usuario_id='" + idmesero + "' and mesa_nomesa='" + numeroDeMesa + "'");
            resultados.next();

            int idDeComanda = resultados.getInt(1);
            ids[0] = idDeComanda;
            int idChef = obtenerIdRecomendacionDeChef();
            ids[1] = idChef;

            //for (int i = 0; i < comandasPorApuntar; i++) {
            consulta = conexion.createStatement();
            strQuery = "INSERT IGNORE INTO comanda_has_recomendacion_del_chef VALUES('" + idDeComanda + "','" + getIDMesero(mesero) + "','" + numeroDeMesa + "','" + idChef + "','" + recomendacionesDeChefPorApuntar + "')";
            //TODO agregarle cantidad y solo meterle cuantas hay
            System.out.println(strQuery);
            consulta.executeUpdate(strQuery);
            //}

            //resultados = consulta.executeQuery("SELECT LAST_INSERT_ID()");// lo inserta solo la primera vez
            //consultar! D:
        } catch (SQLException ex) {
            Logger.getLogger(ServicioDeAccesoABaseDeDatos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

        }
        return ids;
    }

    public Vector<String> obtenerCategoriasDePlatillos() {
        conectarABaseDeDatos();
        Vector<String> categoriasDePlatillos = null;
        int idIngrediente_enesimo = -1;
        try {
            consulta = conexion.createStatement();
            resultados = consulta.executeQuery("SELECT DISTINCT categoria FROM platillo");

            resultados.last();//nos pasamos al último
            ResultSetMetaData rsmd = resultados.getMetaData();
            int numCols = rsmd.getColumnCount();
            System.out.println("Columnas del resultado" + numCols);

            int numFils = resultados.getRow();
            if (numFils != 0) {
                categoriasDePlatillos = new Vector<String>();
                resultados.beforeFirst();//mover antes de empezar
                //resultados.next();
                while (resultados.next()) {
                    String categoria = resultados.getString(1);
                    System.out.println("agregando categoria:" + categoria);
                    categoriasDePlatillos.add(categoria);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(ServicioDeAccesoABaseDeDatos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

        }
        return categoriasDePlatillos;
    }

    public Comanda obtenerComandaExistenteDeMesero(int numeroDeMesa, Mesero mesero) {
        //en sopas guardar el nombre, en el arreglo el precio y el iva
        conectarABaseDeDatos();
        //Vector<Mesa> mesas = new Vector<Mesa>();
        Comanda c = new Comanda();
        try {
            consulta = conexion.createStatement();
            Date hoy = new Date();
            String strQuery = "SELECT * FROM comanda INNER JOIN mesa ON (comanda.mesa_nomesa = mesa.nomesa) INNER JOIN usuario ON (comanda.usuario_id = usuario.id) WHERE mesa.nomesa = '" + numeroDeMesa + "' AND mesa.estado = '" + MESA_OCUPADA + "' AND usuario.nombre = '" + mesero.getNombre() + "'";
            System.out.println(strQuery);
            resultados = consulta.executeQuery(strQuery);
            //asumimos que solamente debe haber un resultado
            resultados.last();//nos pasamos al último
            ResultSetMetaData rsmd = resultados.getMetaData();
            int numCols = rsmd.getColumnCount();
            System.out.println("Columnas del resultado" + numCols);

            int numFils = resultados.getRow();

            // int j = 0;
            resultados.beforeFirst();//mover antes de empezar

            while (resultados.next()) { //mientras haya resultados
//
                int idcomanda = resultados.getInt(1);
//int estado =resultados.getInt(3);
                double precio = resultados.getDouble(4);
                String observaciones = resultados.getString(5);
                c.setNumeroDePedido(idcomanda);
                c.setPrecio(precio);
                c.setCerrada(false);

                c.setQuienTomoLaOrden(mesero);
                c.setOvservaciones(observaciones);
                c.setPlatillos(obtenerPlatillosDeComanda(idcomanda));
                Date fechaDeComanda = resultados.getDate(2);//TODO verificar error al guardar la fecha en comanda
                c.setFechaDePedido(fechaDeComanda);
            }
            return c;
        } catch (SQLException ex) {
            c.setFechaDePedido(new Date());//en caso de error le asignamos el dí de hoy
            Logger.getLogger(ServicioDeAccesoABaseDeDatos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return c;
        }
    }

    /**
     * Nos permite saber que cuentas se pagaron hoy
     */
    public Vector<Comanda> obTenerCuentasPagadas() {
        //en sopas guardar el nombre, en el arreglo el precio y el iva
        conectarABaseDeDatos();
        int estado = -1;
        try {
            consulta = conexion.createStatement();
            Date hoy = new Date();
            resultados = consulta.executeQuery("SELECT * FROM comanda WHERE fecha='" + new SimpleDateFormat("dd/MM/YYYY").format(hoy) + "'");
            //estado = rs.getInt("estado");
            resultados.last();//nos pasamos al último
            ResultSetMetaData rsmd = resultados.getMetaData();
            int numCols = rsmd.getColumnCount();
            System.out.println("Columnas del resultado" + numCols);

            int numFils = resultados.getRow();

            // int j = 0;
            resultados.beforeFirst();//mover antes de empezar
            while (resultados.next()) { //mientras haya resultados

                Double[] obj = new Double[numCols - 1];
                for (int i = 1; i < numCols - 1; i++) {
                    System.out.println("Probando con columna " + (i + 2)); //1 id 2 nombre 3 precio 4 iva 5 categoría
                    String resultado = resultados.getString(i + 2);
                    System.out.println(resultado);
                    obj[i] = Double.parseDouble(resultado);
                    //obj[j][i]=rs.getObject(i+1);
                }
                String nombrePlatillo = resultados.getString(2);
                System.out.println("nombre del platillo:" + nombrePlatillo);
                // datosplatillos.put(nombrePlatillo, obj);// put nombre platillo, [precio, iva]

            }

            //TODO ciclar todos los resultados
        } catch (SQLException ex) {
            Logger.getLogger(ServicioDeAccesoABaseDeDatos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

        }
        return null;
    }

    /**
     * Nos permite saber cuentas pendientes de pagar de este día
     */
    public Vector<Comanda> obtenerCuentasParaPagar() {
        //en sopas guardar el nombre, en el arreglo el precio y el iva
        conectarABaseDeDatos();
        int estado = -1;
        try {
            consulta = conexion.createStatement();
            Date hoy = new Date();
            resultados = consulta.executeQuery("SELECT * FROM comanda WHERE fecha='" + new SimpleDateFormat("dd/MM/YYYY").format(hoy) + "'");
            //estado = rs.getInt("estado");
            resultados.last();//nos pasamos al último
            ResultSetMetaData rsmd = resultados.getMetaData();
            int numCols = rsmd.getColumnCount();
            System.out.println("Columnas del resultado" + numCols);

            int numFils = resultados.getRow();

            // int j = 0;
            resultados.beforeFirst();//mover antes de empezar
            while (resultados.next()) { //mientras haya resultados

                Double[] obj = new Double[numCols - 1];
                for (int i = 1; i < numCols - 1; i++) {
                    System.out.println("Probando con columna " + (i + 2)); //1 id 2 nombre 3 precio 4 iva 5 categoría
                    String resultado = resultados.getString(i + 2);
                    System.out.println(resultado);
                    obj[i] = Double.parseDouble(resultado);
                    //obj[j][i]=rs.getObject(i+1);
                }
                String nombrePlatillo = resultados.getString(2);
                System.out.println("nombre del platillo:" + nombrePlatillo);
                //datosplatillos.put(nombrePlatillo, obj);// put nombre platillo, [precio, iva]

            }

            //TODO ciclar todos los resultados
        } catch (SQLException ex) {
            Logger.getLogger(ServicioDeAccesoABaseDeDatos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

        }
        return null;
    }

    public void obtenerPlatillosPorCategoría(String catego, Hashtable<String, Double[]> datosplatillos) {
        //en sopas guardar el nombre, en el arreglo el precio y el iva
        conectarABaseDeDatos();
        int estado = -1;
        try {
            consulta = conexion.createStatement();
            resultados = consulta.executeQuery("SELECT id, nombre, precio, iva FROM platillo where categoria ='" + catego + "'");
            //estado = rs.getInt("estado");
            resultados.last();//nos pasamos al último
            ResultSetMetaData rsmd = resultados.getMetaData();
            int numCols = rsmd.getColumnCount();
            System.out.println("Columnas del resultado" + numCols);

            int numFils = resultados.getRow();

            // int j = 0;
            resultados.beforeFirst();//mover antes de empezar
            while (resultados.next()) { //mientras haya resultados

                Double[] obj = new Double[numCols - 1];
                for (int i = 1; i < numCols - 1; i++) {
                    System.out.println("Probando con columna " + (i + 2)); //1 id 2 nombre 3 precio 4 iva 5 categoría
                    String resultado = resultados.getString(i + 2);
                    System.out.println(resultado);
                    obj[i] = Double.parseDouble(resultado);
                    //obj[j][i]=rs.getObject(i+1);
                }
                String nombrePlatillo = resultados.getString(2);
                System.out.println("nombre del platillo:" + nombrePlatillo);
                datosplatillos.put(nombrePlatillo, obj);// put nombre platillo, [precio, iva]
                // j++;

            }

            //TODO ciclar todos los resultados
        } catch (SQLException ex) {
            Logger.getLogger(ServicioDeAccesoABaseDeDatos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

        }
    }

    private int obtenerIdPlatillo(String nombreDePlatillo) {
        conectarABaseDeDatos();
        int id = -1;
        try {
            consulta = conexion.createStatement();
            resultados = consulta.executeQuery("SELECT id FROM platillo WHERE nombre = '" + nombreDePlatillo + "'");
            id = resultados.getInt("platillo_id");
        } catch (SQLException ex) {
            Logger.getLogger(ServicioDeAccesoABaseDeDatos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

        }
        return id;
    }

    private int obtenerIDDePlatillo(String nombrePlatillo) {
        conectarABaseDeDatos();
        int i = -1;
        try {
            consulta = conexion.createStatement();
            resultados = consulta.executeQuery("SELECT id FROM platillo WHERE nombre = '" + nombrePlatillo + "'");
            resultados.next();
            i = resultados.getInt("id");
        } catch (SQLException ex) {
            Logger.getLogger(ServicioDeAccesoABaseDeDatos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

        }
        return i;
    }

    private int obtenerIDDeUsuario(String usuario) {
        conectarABaseDeDatos();
        int id = -1;
        Ingrediente i = null;
        try {
            consulta = conexion.createStatement();
            resultados = consulta.executeQuery("SELECT id FROM usuario WHERE nombre = '" + usuario + "'");
            resultados.next();
            id = resultados.getInt("id");

        } catch (SQLException ex) {
            Logger.getLogger(ServicioDeAccesoABaseDeDatos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

        }
        return id;
    }

    public int obtenerIdRecomendacionDeChef() {
        conectarABaseDeDatos();
        int id = -1;
        try {
            Date hoy = new Date();
            consulta = conexion.createStatement();
            String consultaTexto = "SELECT idrecomendacion_del_chef FROM recomendacion_del_chef WHERE fecha = '" + new SimpleDateFormat("YYYY/MM/dd").format(hoy) + "'";
            System.out.println(consultaTexto);
            resultados = consulta.executeQuery(consultaTexto);
            if (resultados.next()) {
                id = resultados.getInt("idrecomendacion_del_chef");
                System.out.println("id de recomendación de chef:" + id);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ServicioDeAccesoABaseDeDatos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

        }
        return id;
    }

    public Vector<Ingrediente> obtenerIngredientesDePlatillo(String nombreDePlatillo) {
        //en sopas guardar el nombre, en el arreglo el precio y el iva
        conectarABaseDeDatos();
        Vector<Ingrediente> ingredientesFelices = null;
        int idIngrediente_enesimo = -1;
        try {
            consulta = conexion.createStatement();
            //buscarPlatillo
            int idPlatillo = obtenerIdPlatillo(nombreDePlatillo);
            resultados = consulta.executeQuery("SELECT ingrediente_id FROM platillo_has_ingrediente WHERE platillo_id='" + idPlatillo + "'");

            resultados.last();//nos pasamos al último
            ResultSetMetaData rsmd = resultados.getMetaData();
            int numCols = rsmd.getColumnCount();
            System.out.println("Columnas del resultado" + numCols);

            int numFils = resultados.getRow();
            if (numFils != 0) {
                ingredientesFelices = new Vector<Ingrediente>();
                resultados.beforeFirst();//mover antes de empezar
                while (resultados.next()) {
                    idIngrediente_enesimo = resultados.getInt(1);
                    Ingrediente ingrediente = obtenerIngredientePorID(idIngrediente_enesimo);
                    ingredientesFelices.add(ingrediente);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(ServicioDeAccesoABaseDeDatos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

        }
        return ingredientesFelices;
    }

    private Ingrediente obtenerIngredientePorID(int idIngrediente) {
        conectarABaseDeDatos();
        Ingrediente i = null;
        try {
            consulta = conexion.createStatement();
            resultados = consulta.executeQuery("SELECT * FROM ingrediente WHERE id = '" + idIngrediente + "'");
            resultados.next();
            String nombre = resultados.getString("nombre");
            Date caducidad = new Date(resultados.getString("caducidad"));
            int vecesUsable = resultados.getInt("veces_usable");
            String descripcion = resultados.getString("descripcion");
            i = new Ingrediente();
            i.setNombre(nombre);
            i.setFechaDeCaducidad(caducidad);
            i.setVecesQueSePuedeUsar(vecesUsable);
            i.setDescripcion(descripcion);
        } catch (SQLException ex) {
            Logger.getLogger(ServicioDeAccesoABaseDeDatos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

        }
        return i;
    }

    public Vector<Mesa> obtenerMesas() {
        //en sopas guardar el nombre, en el arreglo el precio y el iva
        conectarABaseDeDatos();
        Vector<Mesa> mesas = new Vector<Mesa>();
        //int estado = -1;
        try {
            consulta = conexion.createStatement();
            Date hoy = new Date();
            String strQuery = "SELECT * FROM mesa ";
            System.out.println(strQuery);
            resultados = consulta.executeQuery(strQuery);
            resultados.last();//nos pasamos al último
            ResultSetMetaData rsmd = resultados.getMetaData();
            int numCols = rsmd.getColumnCount();
            System.out.println("Columnas del resultado" + numCols);

            int numFils = resultados.getRow();

            // int j = 0;
            resultados.beforeFirst();//mover antes de empezar

            while (resultados.next()) { //mientras haya resultados

                int nombremesa = resultados.getInt(1);
                int estadomesa = resultados.getInt(2);
                System.out.println("Mesa:" + nombremesa);
                System.out.println("edo:" + estadomesa);
                Mesa m = new Mesa();
                m.setNumeroDeMesa(nombremesa);
                m.setEstadoMesa(estadomesa);
                mesas.add(m);
            }
            return mesas;
        } catch (SQLException ex) {
            Logger.getLogger(ServicioDeAccesoABaseDeDatos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

        }
        return null;
    }

    private ArrayList<Platillo> obtenerPlatillosDeComanda(int idcomanda) {
        ArrayList<Platillo> datosplatillos = new ArrayList<Platillo>();
        //en sopas guardar el nombre, en el arreglo el precio y el iva
        conectarABaseDeDatos();
        int estado = -1;
        try {
            consulta = conexion.createStatement();
            String strConsulta = "SELECT platillo.nombre, platillo.precio, platillo.iva, platillo.categoria, comanda_has_platillo.cantidad FROM comanda INNER JOIN comanda_has_platillo ON (comanda.idComanda = comanda_has_platillo.Comanda_idComanda) INNER JOIN platillo ON (comanda_has_platillo.platillo_id = platillo.id) WHERE comanda.idComanda = '" + idcomanda + "'";
            System.out.println(strConsulta);
            resultados = consulta.executeQuery(strConsulta);
            //estado = rs.getInt("estado");
            resultados.last();//nos pasamos al último
            ResultSetMetaData rsmd = resultados.getMetaData();
            int numCols = rsmd.getColumnCount();
            System.out.println("Columnas del resultado" + numCols);

            int numFils = resultados.getRow();
            System.out.println("Filas:" + numFils);

            // int j = 0;
            resultados.beforeFirst();//mover antes de empezar

            while (resultados.next()) { //mientras haya resultados

                String nombrePlatillo = resultados.getString(1);
                System.out.println("nombre del platillo:" + nombrePlatillo);
                double precio = resultados.getDouble(2);
                double iva = resultados.getDouble(3);
                String categoria = resultados.getString(4);
                int cantidad = resultados.getInt(5);
                Platillo p = new Platillo();
                p.setNombrePlatillo(nombrePlatillo);
                p.setPrecio(precio);
                p.setIva(iva);
                p.setCategoria(categoria);
                p.setCantidad(cantidad);
                datosplatillos.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ServicioDeAccesoABaseDeDatos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

        }
        return datosplatillos;
    }

    public void obtenerPlatillosDeRecomendacionDeChef(int idRecomendacion, Hashtable<String, Double[]> datosplatillos) {
        //en sopas guardar el nombre, en el arreglo el precio y el iva
        conectarABaseDeDatos();
        int estado = -1;
        try {
            consulta = conexion.createStatement();
            String strConsulta = "SELECT id, nombre, precio, iva FROM recomendacion_del_chef_has_platillo INNER JOIN platillo ON (recomendacion_del_chef_has_platillo.platillo_id = platillo.id) WHERE recomendacion_del_chef_idrecomendacion_del_chef = '" + idRecomendacion + "'";
            System.out.println(strConsulta);
            resultados = consulta.executeQuery(strConsulta);
            //estado = rs.getInt("estado");
            resultados.last();//nos pasamos al último
            ResultSetMetaData rsmd = resultados.getMetaData();
            int numCols = rsmd.getColumnCount();
            System.out.println("Columnas del resultado" + numCols);

            int numFils = resultados.getRow();
            System.out.println("Filas:" + numFils);

            // int j = 0;
            resultados.beforeFirst();//mover antes de empezar

            while (resultados.next()) { //mientras haya resultados

                Double[] obj = new Double[numCols - 1];
                for (int i = 1; i < numCols - 1; i++) {
                    System.out.println("Probando con columna " + (i + 2)); //1 id 2 nombre 3 precio 4 iva 5 categoría
                    String resultado = resultados.getString(i + 2);
                    System.out.println(resultado);
                    obj[i] = Double.parseDouble(resultado);
                    //obj[j][i]=rs.getObject(i+1);
                }
                String nombrePlatillo = resultados.getString(2);
                System.out.println("nombre del platillo:" + nombrePlatillo);
                datosplatillos.put(nombrePlatillo, obj);// put nombre platillo, [precio, iva]
                // j++;

            }

            //TODO ciclar todos los resultados
        } catch (SQLException ex) {
            Logger.getLogger(ServicioDeAccesoABaseDeDatos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

        }
    }

    public void obtenerVentasDeHoy() {
        conectarABaseDeDatos();
        try {
            consulta = conexion.createStatement();
            Date hoy = new Date();
            resultados = consulta.executeQuery("SELECT * FROM comanda WHERE fecha='" + new SimpleDateFormat("dd/MM/YYYY").format(hoy) + "'");

            resultados.last();//nos pasamos al último
            ResultSetMetaData rsmd = resultados.getMetaData();
            int numCols = rsmd.getColumnCount();
            System.out.println("Columnas del resultado" + numCols);

            int numFils = resultados.getRow();

            // int j = 0;
            resultados.beforeFirst();//mover antes de empezar
            while (resultados.next()) { //mientras haya resultados

                Double[] obj = new Double[numCols - 1];
                for (int i = 0; i < numCols - 1; i++) {
                    System.out.println("Probando con columna " + (i + 2)); //1 id 2 nombre 3 precio 4 iva 5 categoría
                    //String resultado = resultados.getString(i + 2);
                    //System.out.println(resultado);
                    //obj[i] = Double.parseDouble(resultado);
                    //obj[j][i]=rs.getObject(i+1);
                }
                //TODO consultar comandas del día y guardarlas en un vextor o algo similar
                //String nombrePlatillo = resultados.getString(2);
                //System.out.println("nombre del platillo:" + nombrePlatillo);
                //datosplatillos.put(nombrePlatillo, obj);// put nombre platillo, [precio, iva]
            }
        } catch (SQLException ex) {
            Logger.getLogger(ServicioDeAccesoABaseDeDatos.class.getName()).log(Level.SEVERE, null, ex);

        } finally {

        }
    }

    public boolean QuitarRecomendacionDeComanda(int id) {
        conectarABaseDeDatos();
        try {
            consulta = conexion.createStatement();
            consulta.executeUpdate("DELETE FROM comanda_has_recomendacion_del_chef WHERE recomendacion_del_chef_idrecomendacion_del_chef = '" + id + "'");
        } catch (SQLException ex) {
            Logger.getLogger(ServicioDeAccesoABaseDeDatos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } finally {

            return true;
        }
    }

    public void quitarPlatillo(String nombrePlatillo) {
        conectarABaseDeDatos();
        try {
            consulta = conexion.createStatement();
            resultados = consulta.executeQuery("DELETE FROM platillo WHERE nombre ='" + nombrePlatillo + "'");
        } catch (SQLException ex) {
            Logger.getLogger(ServicioDeAccesoABaseDeDatos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

        }
    }

    public void registrarPagoEnBD(int id) {
        conectarABaseDeDatos();
        try {
            consulta = conexion.createStatement();
            resultados = consulta.executeQuery("UPDATE comanda SET cerrada = '2' WHERE idComanda = '" + id + "'");
        } catch (SQLException ex) {
            Logger.getLogger(ServicioDeAccesoABaseDeDatos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

        }
    }

    public double consultarVentasPorMesero(Mesero mesero) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public double obtenerPrecioDeRecomendacionDelChefDelDia() {
        int idRecomendacion = obtenerIdRecomendacionDeChef();
        conectarABaseDeDatos();
        double precio = 0;
        try {
            Date hoy = new Date();
            consulta = conexion.createStatement();
            String consultaTexto = "SELECT precio FROM recomendacion_del_chef WHERE fecha = '" + new SimpleDateFormat("YYYY/MM/dd").format(hoy) + "'";
            System.out.println(consultaTexto);
            resultados = consulta.executeQuery(consultaTexto);
            if (resultados.next()) {
                precio = resultados.getDouble(1);
                System.out.println("id de recomendación de chef:" + precio);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ServicioDeAccesoABaseDeDatos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

        }
        return precio;
    }

    /**
     * Método que permite darle el rol de administrador a un usuario dado
     *
     * @param usuario el id del usuario
     */
    public void agregarPrivilegiosDeAdministrador(String usuario) {
        int id = obtenerIDDeUsuario(usuario);
        conectarABaseDeDatos();
        //TODO cambiar el diseño de bd para que el id sea autoincremental
        try {
            consulta = conexion.createStatement();
            consulta.executeUpdate("INSERT INTO roles VALUES('" + id + "','Administrador' )");
        } catch (SQLException ex) {
            Logger.getLogger(ServicioDeAccesoABaseDeDatos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

        }
    }

    public void quitarPlatilloDeSugerenciaDelChef(Platillo p) {
        conectarABaseDeDatos();
        try {
            consulta = conexion.createStatement();
            if (p.getCantidad() > 1) {
                //TODO auitale uno a la cantidad de platillos en la BD y en el objeto
            } else {
                resultados = consulta.executeQuery("DELETE FROM sugerencia_del_chef_has_platillo WHERE nombre ='" + p.getNombrePlatillo() + "'");
            }
        } catch (SQLException ex) {
            Logger.getLogger(ServicioDeAccesoABaseDeDatos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

        }
    }

    class Ticket { //Ticket attribute content
        //TODO probar la impresión física de ticket

        private String contentTicket = "::" + (char) 27 + (char) 112 + (char) 0 + (char) 10 + (char) 100
                + "ACCESORIOS{{nameLocal}}::\n"
                + "Periban.Mich.{{dateTime}}\n"
                + "EXPEDIDO EN: {{expedition}}\n"
                + "Emiliano Zapata Col. Centro \n"
                + "=============================\n"
                + "Periban. Mich. {{dateTime}}\n"
                + "RFC: XAXX010101000\n"
                + "Caja # {{box}} \n"
                + "Ticket # {{ticket}}\n"
                + "LE ATENDIO: {{cajero}}\n"
                + "{{dateTime}}\n"
                + "=============================\n"
                + "{{items}}\n"
                + "=============================\n"
                + "SUBTOTAL: {{subTotal}}\n"
                + "IVA: {{tax}}\n"
                + "TOTAL: {{total}}\n\n"
                + "RECIBIDO: {{recibo}}\n"
                + "CAMBIO: {{change}}\n\n"
                + "=============================\n"
                + "GRACIAS POR SU COMPRA…\n"
                + "ESPERAMOS SU VISITA NUEVAMENTE\n"
                + "\t::{{nameLocal}}::\n"
                + "\n"
                + "\n\n\n ";

//El constructor que setea los valores a la instancia
        Ticket(String nameLocal, String expedition, String box, String ticket, String caissier, String dateTime, String items, String subTotal, String tax, String total) {
            this.contentTicket = this.contentTicket.replace("{{nameLocal}}", nameLocal);
            this.contentTicket = this.contentTicket.replace("{{expedition}}", expedition);
            this.contentTicket = this.contentTicket.replace("{{box}}", box);
            this.contentTicket = this.contentTicket.replace("{{ticket}}", ticket);
            this.contentTicket = this.contentTicket.replace("{{cajero}}", caissier);
            this.contentTicket = this.contentTicket.replace("{{dateTime}}", dateTime);
            this.contentTicket = this.contentTicket.replace("{{items}}", items);
            this.contentTicket = this.contentTicket.replace("{{subTotal}}", subTotal);
            this.contentTicket = this.contentTicket.replace("{{tax}}", tax);
            this.contentTicket = this.contentTicket.replace("{{total}}", total);
        }

        // public void print() throws IOException {
        //TODO mandar cadena a imprimir
        //}
    }

    public static void main(String[] args) {
        ServicioDeAccesoABaseDeDatos datos = new ServicioDeAccesoABaseDeDatos();
        datos.conectarABaseDeDatos();
        if (datos.login("master", "ofpuppets")) {
            System.out.println("usuario hallado");
        } else {
            System.out.println("usuario no hallado");
        }
    }
}

//}
