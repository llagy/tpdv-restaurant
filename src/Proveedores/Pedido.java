package Proveedores;

import Inventario.almacen.Ingrediente; 
import java.util.ArrayList; 

// <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
// #[regen=yes,id=DCE.73FBA489-1342-DB7B-2048-7095E99AF57F]
// </editor-fold> 
public class Pedido {

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.0E63C81B-80F1-0507-7144-F9D83631F7E5]
    // </editor-fold> 
    private ArrayList<Ingrediente> ingredientesDePedido;

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.471C9217-9DDE-208F-8B79-594CDC5FA759]
    // </editor-fold> 
    public Pedido () {
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.8A99338F-6049-C660-B72E-D05AD7401FC6]
    // </editor-fold> 
    public ArrayList<Ingrediente> getIngredientesDePedido () {
        return ingredientesDePedido;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.0F473465-E30E-BB71-EC6F-8905F392289A]
    // </editor-fold> 
    public void setIngredientesDePedido (ArrayList<Ingrediente> val) {
        this.ingredientesDePedido = val;
    }

}

