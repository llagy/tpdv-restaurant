package Proveedores;


// <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
// #[regen=yes,id=DCE.0331D991-45E1-5C80-F7BF-BC11AC1026C9]
// </editor-fold> 
public class Proveedor {

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.83CBB29D-39AC-E9D6-C64E-224BC9E9856D]
    // </editor-fold> 
    private String nombreORazonSocial;

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.F9EF903A-740A-2C62-3E9F-EAB82700D19F]
    // </editor-fold> 
    public Proveedor () {
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.DBB724E8-BB06-9803-4915-BDF4CD1B71E1]
    // </editor-fold> 
    public String getNombreORazonSocial () {
        return nombreORazonSocial;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.D1B47D6A-A1C4-241A-3E0B-1A65A59906DF]
    // </editor-fold> 
    public void setNombreORazonSocial (String val) {
        this.nombreORazonSocial = val;
    }

}

