package Inventario.almacen;

import java.util.Date;

public class Ingrediente {

    private String nombre;
    private int vecesQueSePuedeUsar;
    private Date fechaDeCaducidad;
    private String descripcion;

    public Ingrediente() {
    }

    public Date getFechaDeCaducidad() {
        return fechaDeCaducidad;
    }

    public void setFechaDeCaducidad(Date val) {
        this.fechaDeCaducidad = val;
    }

    public int getVecesQueSePuedeUsar() {
        return vecesQueSePuedeUsar;
    }

    public void setVecesQueSePuedeUsar(int val) {
        this.vecesQueSePuedeUsar = val;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

}
