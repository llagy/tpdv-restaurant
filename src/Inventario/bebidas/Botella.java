package Inventario.bebidas;


// <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
// #[regen=yes,id=DCE.F54789B4-12A1-EDE8-AE96-5829B026F970]
// </editor-fold> 
public class Botella {

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.2C1A2903-CF96-F84F-5F9C-BCA11741FBCC]
    // </editor-fold> 
    private int marca;

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.BD87DAA3-3B9A-EA9E-1341-5555062CA3B4]
    // </editor-fold> 
    public Botella () {
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.BB39AA52-7E2B-408F-5DA5-333197BEEB88]
    // </editor-fold> 
    public int getMarca () {
        return marca;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.31830CDA-A556-4DA9-9F7C-95A80713B6ED]
    // </editor-fold> 
    public void setMarca (int val) {
        this.marca = val;
    }

}

