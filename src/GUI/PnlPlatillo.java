/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Pedidos.Platillo;
import javax.swing.JPanel;

/**
 *
 * @author Administrador
 */
public class PnlPlatillo extends JPanel {

    Platillo platillo = new Platillo();

    /**
     * Ruta donde se encuetra la foto a mostrar
     */
    String rutaFoto;

    /**
     * Creates new form PnlPlatillo
     */
    public PnlPlatillo() {
        initComponents();
        if (rutaFoto != null || !"".equals(rutaFoto)) {

            // rutaFoto = "/GUI/res/ciaoicono.jpg";
        } else {
            System.out.println("Poniendo foto...");
            ponerFoto();
        }
        //else {
        // ponerFotoPorDefecto();
        //}
    }

    public String getNombrePlatillo() {
        return platillo.getNombrePlatillo();
    }

    public void setNombrePlatillo(String nombrePlatillo) {
        lblNombre.setText(nombrePlatillo);
        platillo.setNombrePlatillo(nombrePlatillo);
    }

    public double getPrecio() {
        return platillo.getPrecio();
    }

    public void setPrecio(double precio) {
        lblPrecio.setText("$" + precio);
        platillo.setPrecio(precio);
    }

    public double getIva() {
        return platillo.getIva();
    }

    public void setIva(double iva) {
        lblTax.setText("$" + iva);
        platillo.setIva(iva);
    }

    public String getRutaFoto() {
        return rutaFoto;
    }

    public void setRutaFoto(String rutaFoto) {
        this.rutaFoto = rutaFoto;
        ponerFoto();
    }

    public String getCategoria() {
        return platillo.getCategoria();
    }

    public void setCategoria(String categoria) {
        platillo.setCategoria(categoria);
    }

    private void ponerFotoPorDefecto() {
        imgFoto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/GUI/res/ciaoicono.jpg")));
    }

    private void ponerFoto() {
        imgFoto.setIcon(new javax.swing.ImageIcon(rutaFoto));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblNombre = new javax.swing.JLabel();
        imgFoto = new javax.swing.JLabel();
        lblPrecio = new javax.swing.JLabel();
        lblprice = new javax.swing.JLabel();
        lblIva = new javax.swing.JLabel();
        lblTax = new javax.swing.JLabel();

        setLayout(new java.awt.GridLayout(3, 2));

        lblNombre.setText("Nombre");
        add(lblNombre);

        imgFoto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/GUI/res/ciaoicono.jpg"))); // NOI18N
        add(imgFoto);

        lblPrecio.setText("Precio:");
        add(lblPrecio);

        lblprice.setText("$0.00");
        add(lblprice);

        lblIva.setText("IVA:");
        add(lblIva);

        lblTax.setText("$0.00");
        add(lblTax);
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel imgFoto;
    private javax.swing.JLabel lblIva;
    private javax.swing.JLabel lblNombre;
    private javax.swing.JLabel lblPrecio;
    private javax.swing.JLabel lblTax;
    private javax.swing.JLabel lblprice;
    // End of variables declaration//GEN-END:variables
}
