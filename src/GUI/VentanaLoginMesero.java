package GUI;

import Administracion.Mesero;
import SistemaDeKioscos.ServicioDeAccesoABaseDeDatos;
import SistemaDeKioscos.ServicioDeNotificacion;
import java.sql.Connection;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import org.jvnet.substance.SubstanceLookAndFeel;
import org.jvnet.substance.api.SubstanceConstants;
import org.jvnet.substance.api.SubstanceSkin;
import org.jvnet.substance.skin.AutumnSkin;
import org.jvnet.substance.skin.BusinessBlackSteelSkin;
import org.jvnet.substance.skin.BusinessBlueSteelSkin;
import org.jvnet.substance.watermark.SubstanceImageWatermark;

public class VentanaLoginMesero extends javax.swing.JFrame {

    public static Connection con;
    private ServicioDeAccesoABaseDeDatos baseDeDatos;
    private VentanaMesero comandas;
    VentanaAdministracion frm;
    Mesero m = new Mesero();
    String pass = "";
    //teclado en pantalla
    PanelTeclado tecladoEnPantalla = new PanelTeclado();
    JPopupMenu pop = new JPopupMenu();

    public VentanaLoginMesero() {
        initComponents();
        //agregamos el teclado en pantalla

        //esto centra la pantalla
        this.setLocationRelativeTo(null);

        // da el autofocus al inicio
        txtUsuario.requestFocusInWindow();
        txtPass.transferFocus();
        baseDeDatos = new ServicioDeAccesoABaseDeDatos();
        //mandar manseja de bienvenida
        ServicioDeNotificacion.bienvenida();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtUsuario = new javax.swing.JTextField();
        txtPass = new javax.swing.JPasswordField();
        btnEntrar = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Restaurante Ciao Bella Tlaxcala");
        setAlwaysOnTop(true);
        setBounds(new java.awt.Rectangle(0, 0, 800, 600));
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel2.setFont(new java.awt.Font("Lucida Handwriting", 3, 24)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 204, 0));
        jLabel2.setText("USUARIO");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 320, 141, -1));

        jLabel3.setFont(new java.awt.Font("Lucida Handwriting", 3, 24)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 204, 0));
        jLabel3.setText("CLAVE");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(233, 371, 113, -1));

        txtUsuario.setBackground(new java.awt.Color(153, 204, 255));
        txtUsuario.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        txtUsuario.setForeground(new java.awt.Color(0, 102, 204));
        txtUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtUsuarioActionPerformed(evt);
            }
        });
        getContentPane().add(txtUsuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 320, 150, 29));

        txtPass.setBackground(new java.awt.Color(153, 204, 255));
        txtPass.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        txtPass.setForeground(new java.awt.Color(0, 102, 204));
        txtPass.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtPassFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtPassFocusLost(evt);
            }
        });
        txtPass.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPassActionPerformed(evt);
            }
        });
        getContentPane().add(txtPass, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 380, 150, -1));

        btnEntrar.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        btnEntrar.setForeground(new java.awt.Color(255, 102, 0));
        btnEntrar.setText("Entrar");
        btnEntrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEntrarActionPerformed(evt);
            }
        });
        getContentPane().add(btnEntrar, new org.netbeans.lib.awtextra.AbsoluteConstraints(317, 416, 119, 31));

        jLabel4.setFont(new java.awt.Font("Lucida Handwriting", 3, 24)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(51, 102, 255));
        jLabel4.setText("USUARIO");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 320, 141, -1));

        jLabel5.setFont(new java.awt.Font("Lucida Handwriting", 3, 24)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(51, 102, 255));
        jLabel5.setText("CLAVE");
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(233, 371, 113, -1));

        setBounds(0, 0, 804, 600);
    }// </editor-fold>//GEN-END:initComponents

    private void btnEntrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEntrarActionPerformed
////     try{
//       Class.forName("com.mysql.jdbc.Driver");
//            
//            Connection con = DriverManager.getConnection("jdbc:mysql://127.0.0.1/"+ruta,userbd,clavebd);
//            String usuario = jtUsuario.getText();
//     String clave = jtPassword.getText();
//       
//            if (usuario != null && clave!=null) {
//            Statement stmt = con.createStatement();
//            ResultSet rs=stmt.executeQuery("Select * from usuario Where usuario='" + usuario + "' and clave='"+clave+"'");
//            
//            if(rs.next()){
//            String categoria=rs.getString("categoria");
//                if (categoria.equals("administardor")){
//                    administracion frm = new administracion();
//                    frm.setVisible(true);
//                }
//                if (categoria.equals("cajero")){
//                    System.out.println("soy un cajero");
//                } 
//            
//            System.out.println (categoria);
//            System.out.println ("Usuario Aceptado");
//            this.dispose();
//            
//            
//       } else {
//            System.out.println ("Usuario no aceptado");
//            JOptionPane.showMessageDialog(null, "Usuario o clave no reconocido", "Alerta", JOptionPane.ERROR_MESSAGE);
//            jtUsuario.setText("");
//            jtPassword.setText("");
//       }
//         }
//     }
//     catch (Exception e){
//     
//     JOptionPane.showMessageDialog(this,"Sin Conectarse a la base de Datos");
////     }
        loguearMesero();
    }//GEN-LAST:event_btnEntrarActionPerformed

    private void txtUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtUsuarioActionPerformed
        loguearMesero();
    }//GEN-LAST:event_txtUsuarioActionPerformed

    private void txtPassActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPassActionPerformed
        loguearMesero();
    }//GEN-LAST:event_txtPassActionPerformed


    private void txtPassFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtPassFocusGained
        tecladoEnPantalla.setTextField(txtPass);
        pop.add(tecladoEnPantalla);
        pop.pack();
        //pop.setSize(550, 330);
        pop.setVisible(true);
        pop.setLocation(txtPass.getLocationOnScreen().x + 112, txtPass.getLocationOnScreen().y - 1);
    }//GEN-LAST:event_txtPassFocusGained

    private void txtPassFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtPassFocusLost
        pop.setVisible(false);
    }//GEN-LAST:event_txtPassFocusLost

    private void loguearMesero() {
        //TODO verificar que los campos no etén vacíos
        String usuario = txtUsuario.getText();
        String pass = txtPass.getText();
        //conexion cc = new conexion();
        //Connection cn = cc.conexion();
        System.out.println(usuario);
        System.out.println(pass);
        if (usuario != null && pass != null) {
            baseDeDatos.conectarABaseDeDatos();
            //Statement stmt = cn.createStatement();

            if (baseDeDatos.login(usuario, pass)) {
                //TODO Verificar el nivel de acceso
                int privilegio = baseDeDatos.loginUsuario(usuario, pass);
                System.out.println("privilegio:" + privilegio);
                if (privilegio == 3) {
                    comandas = new VentanaMesero(this);
                    comandas.setBaseDeDatos(baseDeDatos);
                    System.out.println("Usuario Aceptado");
                    this.dispose();
                    m.setNombre(usuario);
                    comandas.setMesero(m);
                    pass = "";
                    txtPass.setText(pass);
                    this.setVisible(false);
                    comandas.setVisible(true);
                    //comandas.correTiempo();
                } else if (privilegio == 4) {
                    System.out.println("Bienvnido administrador");
                    int opcoin = JOptionPane.showConfirmDialog(this, "¿Desea entrar al menú de administración?");
                    switch (opcoin) {
                        case JOptionPane.YES_OPTION:
                            setVisible(false);//TODO pasarle esta ventana como parámetro para regresar al cerrar la ventana de administración
                            frm = new VentanaAdministracion();
                            frm.setBaseDeDatos(baseDeDatos);
                            frm.setVisible(true);
                            break;
                        default:
                            comandas = new VentanaMesero(this);
                            comandas.setBaseDeDatos(baseDeDatos);
                            System.out.println("Usuario Aceptado");
                            this.dispose();
                            m.setNombre(usuario);
                            comandas.setMesero(m);
                            pass = "";
                            txtPass.setText(pass);
                            this.setVisible(false);
                            comandas.setVisible(true);
                    }
                }
            } else {
                System.out.println("Usuario no aceptado");
                JOptionPane.showMessageDialog(this, "Usuario o clave no reconocido", "Alerta", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    @Override
    public void setVisible(boolean b) {
        super.setVisible(b);
        SubstanceImageWatermark watermark = new SubstanceImageWatermark("GUI/res/login.jpg");
        watermark.setKind(SubstanceConstants.ImageWatermarkKind.APP_TILE);
        watermark.setOpacity(0.84f);// 设置水印透明度          
        SubstanceSkin skin = new BusinessBlackSteelSkin()//CremeCoffeeSkin()
                .withWatermark(watermark); // 初始化有水印的皮肤
        SubstanceLookAndFeel.setSkin(skin);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            //for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//              //  if ("Nimbus".equals(info.getName())) {
//                //    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                 //   break;
//               // }
//           
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(VentanaLoginMesero.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(VentanaLoginMesero.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(VentanaLoginMesero.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(VentanaLoginMesero.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//        //</editor-fold>
//        //</editor-fold>
//        </editor-fold>
        JFrame.setDefaultLookAndFeelDecorated(true);

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new VentanaLoginMesero().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnEntrar;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPasswordField txtPass;
    private javax.swing.JTextField txtUsuario;
    // End of variables declaration//GEN-END:variables
}
