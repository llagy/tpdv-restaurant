/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Toolkit;

/**
 *
 * @author Administrador
 */
public class ventanaNotificaciones extends javax.swing.JFrame {

    private String AsuntoMensaje;
    private String contenidoMensaje;

    /**
     * Crea una nueva ventana para notificaciones dando la bienvenida
     */
    public ventanaNotificaciones() {
        AsuntoMensaje = "Mensaje de sincronización";
        contenidoMensaje = "Bienvenido";
        initComponents();
        iniciarMuerte();
    }

    public ventanaNotificaciones(String AsuntoMensaje, String contenidoMensaje) {
        this.AsuntoMensaje = AsuntoMensaje;
        this.contenidoMensaje = contenidoMensaje;
        initComponents();
        iniciarMuerte();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {

        pnlBarraDeTitulo3 = new javax.swing.JPanel();
        pnlBotones3 = new javax.swing.JPanel();
        btnExit = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        lblTituloFrame = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();

        setUndecorated(true);

        pnlBarraDeTitulo3.setBackground(new java.awt.Color(0, 0, 0));
        pnlBarraDeTitulo3.setFocusable(false);
        pnlBarraDeTitulo3.setLayout(new java.awt.BorderLayout());

        pnlBotones3.setBackground(new java.awt.Color(0, 0, 0));
        pnlBotones3.setFocusable(false);

        btnExit.setText("x");
        btnExit.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExitActionPerformed(evt);
            }
        });
        pnlBotones3.add(btnExit);

        pnlBarraDeTitulo3.add(pnlBotones3, java.awt.BorderLayout.EAST);

        jPanel2.setBackground(new java.awt.Color(0, 0, 0));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/GUI/res/ciaoicono.jpg"))); // NOI18N
        jPanel2.add(jLabel1);

        lblTituloFrame.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N

        lblTituloFrame.setText(AsuntoMensaje);
        lblTituloFrame.setFocusable(false);
        jPanel2.add(lblTituloFrame);

        pnlBarraDeTitulo3.add(jPanel2, java.awt.BorderLayout.WEST);

        getContentPane().add(pnlBarraDeTitulo3, java.awt.BorderLayout.PAGE_START);

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel2.setText(contenidoMensaje);
        jLabel2.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jPanel1.add(jLabel2);

        getContentPane().add(jPanel1, java.awt.BorderLayout.CENTER);

        pack();
        Dimension scrSize = Toolkit.getDefaultToolkit().getScreenSize();// size of the screen
        Insets toolHeight = Toolkit.getDefaultToolkit().getScreenInsets(getGraphicsConfiguration());// height of the task bar
        setLocation(scrSize.width - getWidth(), scrSize.height - toolHeight.bottom - getHeight());
        setAlwaysOnTop(true);
    }// </editor-fold>                        

    private void btnExitActionPerformed(java.awt.event.ActionEvent evt) {
        dispose();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(ventanaNotificaciones.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new ventanaNotificaciones().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify                     
    private javax.swing.JButton btnExit;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel lblTituloFrame;
    private javax.swing.JPanel pnlBarraDeTitulo3;
    private javax.swing.JPanel pnlBotones3;
    // End of variables declaration                   

    private void iniciarMuerte() {
        new Thread() {
            @Override
            public void run() {
                try {
                    Thread.sleep(5000); // time after which pop up will be disappeared.
                    dispose();
                } catch (InterruptedException e) {
                }
            }
        ;
    }


.start();
    }
}
