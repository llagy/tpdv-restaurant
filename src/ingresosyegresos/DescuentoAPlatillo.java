package ingresosyegresos;

import Pedidos.Platillo; 

// <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
// #[regen=yes,id=DCE.911F05F3-9495-EF45-69A6-E2F05EC0DCB8]
// </editor-fold> 
public class DescuentoAPlatillo extends Descuento {

    /**
     *  <p style="margin-top: 0">
     *        Por si se desea manejar un descuento en ciertos platillos
     *      </p>
     */
    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.EA07236B-A18E-C86E-3A57-1167CA086856]
    // </editor-fold> 
    private Platillo platilloEnDescuento;

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.72F7ADB9-7680-E8E3-A1CA-CC8C02FB02C9]
    // </editor-fold> 
    public DescuentoAPlatillo () {
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.EA16A54C-8390-C06B-C1B1-DDFAB7959AEE]
    // </editor-fold> 
    public double getMontoDescuento () {
        return 0.0;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.A7BEABEB-BC04-4469-2F8F-00F49D1C7EA5]
    // </editor-fold> 
    public void setMontoDescuento (double val) {
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.2586EDC0-3495-D0F5-B79E-38679DC82804]
    // </editor-fold> 
    public Platillo getPlatilloEnDescuento () {
        return platilloEnDescuento;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.E072BF04-8A45-06B7-1BB4-2CDBA4A5507E]
    // </editor-fold> 
    public void setPlatilloEnDescuento (Platillo val) {
        this.platilloEnDescuento = val;
    }

}

