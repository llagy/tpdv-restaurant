package ingresosyegresos;


// <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
// #[regen=yes,id=DCE.07C70A16-A0CF-9801-5DDB-E36FC61A6AB9]
// </editor-fold> 
public class Gasto {

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.0D327062-1F70-A892-6748-6F23922E9401]
    // </editor-fold> 
    private double monto;

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.2A9B1C84-7E6B-7004-2D7B-78323B36F440]
    // </editor-fold> 
    private String nombre;

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.D96ED09D-4892-7D97-B95D-D00DDD74F2C8]
    // </editor-fold> 
    private String descripcion;

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.6290BBBE-C61B-C3BE-294B-CBBBC022FD75]
    // </editor-fold> 
    public Gasto () {
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.6DBCDEDC-5403-B2C7-1067-DB68938129BE]
    // </editor-fold> 
    public String getDescripcion () {
        return descripcion;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.79D65A8D-F9A4-4304-BDDD-9C0DAC550FCD]
    // </editor-fold> 
    public void setDescripcion (String val) {
        this.descripcion = val;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.F75F0703-B14F-335C-D980-07108794586F]
    // </editor-fold> 
    public double getMonto () {
        return monto;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.022800AA-6CF9-6775-E5C1-B6D321D8FA6F]
    // </editor-fold> 
    public void setMonto (double val) {
        this.monto = val;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.43A30A11-CA7D-2002-E939-F3A8A9FAA193]
    // </editor-fold> 
    public String getNombre () {
        return nombre;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.6BE09005-5720-3FA1-BBAD-876E971A7DFC]
    // </editor-fold> 
    public void setNombre (String val) {
        this.nombre = val;
    }

}

