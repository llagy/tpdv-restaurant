package ingresosyegresos;


// <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
// #[regen=yes,id=DCE.E37A78DB-2DC6-7260-26A8-D42CCF2CF71B]
// </editor-fold> 
public class Descuento {

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.7962FB48-8436-0A61-9071-DCBE67850277]
    // </editor-fold> 
    private double montoDescuento;

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.5A4EF996-485B-2691-C78F-6CECDED18D19]
    // </editor-fold> 
    public Descuento () {
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.F082CC20-4367-C7F1-90D3-B9D39A7B54E7]
    // </editor-fold> 
    public double getMontoDescuento () {
        return montoDescuento;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.45A74596-5782-726F-7043-5B1614532BA6]
    // </editor-fold> 
    public void setMontoDescuento (double val) {
        this.montoDescuento = val;
    }

}

