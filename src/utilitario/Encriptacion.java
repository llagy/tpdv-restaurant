package utilitario;

import java.security.MessageDigest;

public final class Encriptacion {

    public static String MD5(String texto) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.reset();
            md.update(texto.getBytes());
            byte bytes[] = md.digest();
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < bytes.length; i++) {
                String hex = Integer.toHexString(0xff & bytes[i]);
                if (hex.length() == 1) {
                    sb.append('0');
                }
                sb.append(hex);
            }
            return sb.toString();
        } catch (Exception e) {
            return null;
        }
    }

    public static void main(String[] args) {
        String saludo = "hola";
        System.out.println(saludo);
        System.out.println("convirtiendo a md5");
        String cifrado = MD5(saludo);
        System.out.println(cifrado);
    }
}
